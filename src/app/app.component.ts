import { Component } from '@angular/core';
import { StockComponent } from './stock/stock.component'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Gestion des stocks';
}
