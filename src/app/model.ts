export class Produit {

    nom: string;
    fournisseur: string;
    emailFour: string;
    listIngredients: [string];
    description: string;
    age: number;
    conditionConservation: string;
    prix: number;
    
    constructor(nom, fournisseur, emailFour, listIngredients, description, age, conditionConservation, prix){
        this.nom = nom;
        this.fournisseur = fournisseur;
        this.emailFour = emailFour;
        this.listIngredients = listIngredients;
        this.description = description;
        this.age = age; 
        this.conditionConservation = conditionConservation;
        this.prix = prix;
    }
}
