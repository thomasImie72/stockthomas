import { Injectable } from '@angular/core';
import { produitsArr } from '../stock';
import { Produit } from '../model';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  constructor() { }

  add(produit: Produit) {
    produitsArr.push(produit);
  }
}
