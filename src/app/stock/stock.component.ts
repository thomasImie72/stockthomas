import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';
import { produitsArr } from '../stock';
import { StockService } from '../services/stock.service';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {

  produits: any = [];

  nomProduitCtrl: FormControl;
  fournisseurCtrl: FormControl;
  ageCtrl: FormControl;
  descriptionCtrl: FormControl;
  
  productForm: FormGroup;

  displayForm: Boolean = false;

  constructor(private stockService: StockService, fb: FormBuilder, private iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    this.produits = produitsArr;

    this.nomProduitCtrl = fb.control('', Validators.required);
    this.fournisseurCtrl = fb.control('', Validators.required);
    this.ageCtrl = fb.control('', Validators.required);
    this.descriptionCtrl = fb.control('', [Validators.required, Validators.maxLength(20)]);

    this.productForm = fb.group({
      nom: this.nomProduitCtrl,
      fournisseur: this.fournisseurCtrl,
      age: this.ageCtrl,
      description: this.descriptionCtrl,
    });
  }

  ngOnInit() {
  }

  remove(name) {
    produitsArr.forEach((item, index) => {
      if (item.nom === name) {
        produitsArr.splice(index, 1);
      }
    });
  }

  reset() {
    this.nomProduitCtrl.setValue('');
    this.fournisseurCtrl.setValue('');
    this.ageCtrl.setValue('');
    this.descriptionCtrl.setValue('');
  }

  register() {
    this.stockService.add(this.productForm.value);
    this.displayForm = false;
  }

  showForm() {
    this.displayForm = this.displayForm ? false : true
  }
}
